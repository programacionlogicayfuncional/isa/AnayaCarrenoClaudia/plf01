(ns plf01.core)
(defn función-<-1
  [a]
  (< a))

(defn función-<-2
  [a b]
  (< a b))

(defn función-<-3
  [a b c]
  (< a b c))
(función-<-1 5)
(función-<-2 10 5)
(función-<-3 1 2 3)

(defn función-<=-1
  [a]
  (<= a))

(defn función-<=-2
  [a b]
  (<= a b))

(defn función-<=-3
  [a b c]
  (<= a b c))
(función-<=-1 5)
(función-<=-2 2 1)
(función-<=-3 0 1 2)

(defn función-==-1
  [a]
  (== a))

(defn función-==-2
  [a b]
  (== b a))

(defn función-==-3
  [a b c]
  (== (* a b) (* b c) c))
(función-==-1 4)
(función-==-2 10 30.3)
(función-==-3 1 1 1)

(defn función->-1
  [a]
  (> a))

(defn función->-2
  [a b]
  (> (+ a 2) (- b 2)))

(defn función->-3
  [a b c]
  (> a b c))
(función->-1 5/3)
(función->-2 5 8)
(función->-3 100 5 7)

(defn función->=-1
  [a]
  (>= a))

(defn función->=-2
  [a b]
  (>= a (- b 10)))

(defn función->=-3
  [a b c]
  (>= a b c))
(función->=-1 30)
(función->=-2 5 100)
(función->=-3 7 6 4)

(defn función-assoc-1
  [as key val]
  (assoc as key val))

(defn función-assoc-2
  [as key val key2 val2]
  (assoc as key val key2 val2))

(defn función-assoc-3
  [as key val key2 val2 key3 val3]
  (assoc as key val key2 val2 key3 val3))

(función-assoc-1 { :uno 1} :dos 2)
(función-assoc-2  {:nombre "Katia" :edad 17} :ocupacion"estudiante" :nacionalidad"Mexicana")
(función-assoc-3 [] 0 "Katia" 1 17 2 "estudiante")


(defn función-concat-1
  [valor1]
  (concat valor1))

(defn función-concat-2
  [as bs]
  (concat as bs))

(defn función-concat-3
  [s as bs]
  (concat (concat  as bs  s)))

(función-concat-1 "adios")
(función-concat-2 [5 6 7] #{9 10 8})
(función-concat-3  "hola" [6 7 8]  {1 10 2 20})

(defn función-conj-1
  [as]
  (conj as))

(defn función-conj-2
  [as bs]
  (conj as bs))

(defn función-conj-3
  [as bs cs]
  (conj as bs cs))

(función-conj-1 [1 2 3 4])
(función-conj-2 [{"ruben" "dario"}] ["karla"])
(función-conj-3 [] {1 2 3 4} '(5 7))

(defn función-cons-1
  [x as]
  (cons x as))

(defn función-cons-2
  [x as]
  (cons x as))

(defn función-cons-3
  [x as]
  (cons x as))

(función-cons-1 [10 80 20] [1 2 3 4 5])
(función-cons-2 [0.1 0.2] {1 [1 2 3] 2 [100 200 300]})
(función-cons-3 10 [[] {40 1} '(1 2)])

(defn función-contains?-1
  [as x]
  (contains? as  x))

(defn función-contains?-2
  [as x]
  (contains? as x))

(defn función-contains?-3
  [bs x]
  (contains? bs x))

(función-contains?-1 {:1 \a :2 \b :3 \c} :1)
(función-contains?-2 {:saludo "hola" :despedida "adios"} :año)
(función-contains?-3 #{"a" "b" [1 2 3]} [1 2 3])

(defn función-count-1
  [as]
  (count as))

(defn función-count-2
  [as bs]
  (count [as bs]))

(defn función-count-3
  [as bs n]
  (count [as bs n]))

(función-count-1 [10 [20 30 40] 100 1000])
(función-count-2 {:1 "hola" :2 \a :3 \e} (list 10 20 30))
(función-count-3 [1 0 5 0 6] ['(\h \o \l \a) 5] 1000)

(defn función-disj-1
  [as n]
  (disj as n))

(defn función-disj-2
  [as n]
  (disj as n))

(defn función-disj-3
  [as bs cs ds]
  (disj #{bs cs} ds as))

(función-disj-1 #{10 20 30} 1)
(función-disj-2 #{"oaxaca" "huatulco" #{\h \o \i \j \m}} "huatulco")
(función-disj-3 [\a \b \c \d] (list "hi" "bye" 10 20 30) {:uno 1 :dos 2 :tres 3} [\a \b \c \d])

(defn función-dissoc-1
  [ns]
  (dissoc ns))

(defn función-dissoc-2
  [as n]
  (dissoc as n))

(defn función-dissoc-3
  [as bs cs n1 n2]
  (dissoc {:a as :b bs :c cs} n1 n2))

(función-dissoc-1 {:uno 1 :dos 2 :tres 3})
(función-dissoc-2 {1.1 1.2 ,1.3 1.4} :1.2)
(función-dissoc-3 ["ardilla" "arbol" "agua"] (list "bufalo" "buho" "burro") #{"caracol" "conejo" "cuervo"} :a :b)

(defn función-distinct-1
  [x y]
  (distinct? x y))

(defn función-distinct-2
  [n n2]
  (distinct? n n2))

(defn función-distinct-3
  [n n1 n2 n3]
  (distinct? n n1 n2 n3))

(función-distinct-1 [1 2 3 4 5] 7)
(función-distinct-2 [10 20 30] #{10 20 30})
(función-distinct-3 4 7 [5 8 9 3 7 7] ["hi" "hi" "hi"])

(defn función-drop-last-1
  [n]
  (drop-last n )) 

(defn función-drop-last-2
  [n as]
  (drop-last n as))

(defn función-drop-last-3
  [n as]
  (drop-last n as))

(función-drop-last-1 [1 2 3 4 5 6 7 8 9 10])
(función-drop-last-2 5  #{1 2 3 4 5 6 7 8 9 10})
(función-drop-last-3 1 [[1 2 3] (list 4 5 6)])

(defn función-empty-1
  [as]
  (empty as))

(defn función-empty-2
  [as]
  (empty as))

(defn función-empty-3
  [as ]
  (empty as))

(función-empty-1 [1 2 3 4 5])
(función-empty-2 {1 2 3 4 5 3})
(función-empty-3 #{1 2 3 4 5})

(defn función-empty?-1
  [as]
  (empty? as))

(defn función-empty?-2
  [as]
  (empty? as))

(defn función-empty?-3
  [as]
  (empty? as))

(función-empty?-1 [#{1 2 3 4} "hi" "bye"])
(función-empty?-2 #{})
(función-empty?-3 (list"hi" "bye" "ooo" [3 00 2 00]))

(defn función-even?-1
  [n]
  (even? n))

(defn función-even?-2
  [n]
  (even? n))

(defn función-even?-3
  [as]
  (even? as))

(función-even?-1 5)
(función-even?-2 (+ 2 1))
(función-even?-3 ( count ["a" "b" 1 2]))

(defn función-false?-1
  [n]
  (false? n))

(defn función-false?-2
  [n]
  (false? n))

(defn función-false?-3
  [n]
  (false? n))

(función-false?-1 (distinct? [1 2 3 4 5 7]))
(función-false?-2 (función-empty-1 [1 2 3 ]))
(función-false?-3 true )

(defn función-find-1
  [as n]
  (find as n))

(defn función-find-2
  [as n]
  (find as n))

(defn función-find-3
  [as n]
  (find as n))

(función-find-1 [:a :b :c :d] 2)
(función-find-2 {:1 "uno" :2 "dos" :3 "tres" :4 "cuatro"} :1)
(función-find-3 {4 5 6 7 8 ["hi" "bye"]} 8)

(defn función-first-1
  [as]
  (first as))

(defn función-first-2
  [as]
  (first as))

(defn función-first-3
  [as]
  (first as))

(función-first-1 [1 2 3 4])
(función-first-2 [{10 5} #{} 7 0])
(función-first-3 #{[10 5 7 0] [1 5 7 0]} )

(defn función-flatten-1
  [as]
  (flatten as ))

(defn función-flatten-2
  [as]
  (flatten as))

(defn función-flatten-3
  [as]
  (flatten as))

(función-flatten-1 ["hola" "adios"])
(función-flatten-2 #{3.1 4.1 5.1} )
(función-flatten-3 (list 3 4 5))

(defn función-frequencies-1
  [as]
  (frequencies as))

(defn función-frequencies-2
  [bs]
  (frequencies bs))

(defn función-frequencies-3
  [cs]
  (frequencies cs))

(función-frequencies-1 ['a 'b 3 7 9 5 'a 3 1 9 1])
(función-frequencies-2 {:a 1 :b 2 :c 4 :d 1 :e 4})
(función-frequencies-3 (list 'h 't 'e 4 'w 5 'h ))

(defn función-get-1
  [as n]
  (get as n))

(defn función-get-2
  [bs n]
  (get bs n))

(defn función-get-3
  [cs n]
  (get cs n))

(función-get-1 [10 20 30 40] 2)
(función-get-2 {:nombre "katy" :edad 17 :ocupacion "estudiante"} :nombre)
(función-get-3 #{"lopez" 1 3.6 'e [1 2 3]} 'e)

(defn función-into-1
  [as]
  (into as))

(defn función-into-2
  [as bs]
  (into as bs))

(defn función-into-3
  [as bs cs]
  (into as bs cs))

(función-into-1 '(1 2 \a \b 3 4))
(función-into-2 #{1 10, 2 20} [5])
(función-into-3 ['h 'o 'l 'a] #{1 2 3 4 5} (list 1 2))

(defn función-key-1
  [as]
  (key as))

(defn función-key-2
  [as]
  (key as))

(defn función-key-3
  [as]
  (key as))

(función-key-1 (first{:a 1 :b 2}))
(función-key-2 (first {:a 1 :b 2 :c 3 :d 4}))
(función-key-3 (first {:a [ 2 3 5] :b [7 9 0]}))

(defn función-keys-1 
  [as] 
  ( keys as))

(defn función-keys-2
  [vs]
  (keys vs))

(defn función-keys-3
  [bs]
  (keys bs))

(función-keys-1 {1 2, 3 4 ,5 6})
(función-keys-2 {:1 [1.4 1.5 ] :2 [2.4 2.5]})
(función-keys-3 {:1(list 1 3 6 9 0 3 5 2) :2 [2.4 2.5]})

(defn función-max-1
  [n]
  (max n))

(defn función-max-2
  [n1 n2]
  (max n1 n2))

(defn función-max-3
  [n1 n2 n3 n4 ]
  (max n1 n2 n3 n4))

(función-max-1 10)
(función-max-2 1 2)
(función-max-3 5 8 7 9)

(defn función-merge-1
  [as]
  (merge as))

(defn función-merge-2
  [as bs]
  (merge as bs))

(defn función-merge-3
  [as bs cs]
  (merge as bs cs))

(función-merge-1 {1 2})
(función-merge-2 #{1 2 3 4 5} {:1 3 :4 7})
(función-merge-3 {:x 1 :y 2} {:y 3 :z 4} {:z 10})

(defn función-min-1
  [n]
  (min n))

(defn función-min-2
  [n1 n2]
  (min n1 n2))

(defn función-min-3
  [n1 n2 n3 n4 n5 n6]
  (min n1 n2 n3 n4 n5 n6))

(función-min-1 10)
(función-min-2 0.5 0.4 )
(función-min-3 10 7.5 8 70/80 0.8 1)

(defn función-neg?-1
  [n]
  (neg? n))

(defn función-neg?-2
  [n]
  (neg? n))

(defn función-neg?-3
  [n]
  (neg? n))

(función-neg?-1 -5) 
(función-neg?-2 (* -2 -2))
(función-neg?-3 (* -1 7/8))

(defn función-nil?-1
  [n]
  (nil? n))

(defn función-nil?-2
  [n]
  (nil? n))

(defn función-nil?-3
  [as]
  (nil? as))

(función-nil?-1 nil)
(función-nil?-2 (/ 10 2))
(función-nil?-3 [1 2 3 4 5 ])

(defn función-not-empty-1
  [as]
  (not-empty as))

(defn función-not-empty-2
  [as]
  (not-empty as))

(defn función-not-empty-3
  [n]
  (not-empty n))

(función-not-empty-1 ["hola" "adios"])
(función-not-empty-2 {1 2, 3 4, 5 6, 10 ['r 'r' 't 'u]})
(función-not-empty-3 [])

(defn función-nth-1
  [as n]
  (nth as n))

(defn función-nth-2
  [as n1 n2]
  (nth as n1 n2))

(defn función-nth-3
  [as n]
  (nth as n))

(función-nth-1 [1 2 3 4 5] 2)
(función-nth-2 [10 9 8 7 6 5] 10 5)
(función-nth-3 [] 10)

(defn función-odd?-1
  [n]
  (odd? n))

(defn función-odd?-2
  [n]
  (odd? n))

(defn función-odd?-3
  [n]
  (odd? n))

(función-odd?-1 5)
(función-odd?-2 (* 2 3)) 
(función-odd?-3 (+ 5 7 10)) 

(defn función-partition-1
  [as n]
  (partition as n))

(defn función-partition-2
  [as n1 n2]
  (partition as n1 n2))

(defn función-partition-3
  [as n1 n2 n3]
  (partition as n1 n2 n3))

(función-partition-1 2 [1 2 3 4 5 6 7 8 9 10] )
(función-partition-2 2 3 [1 2 3 4 5 6 7 8 9 10])
(función-partition-3 2 3 [][1 2 3 4 5 6 7 8 9 10])

(defn función-partition-all-1
  [as]
  (partition-all as))

(defn función-partition-all-2
  [n as]
  (partition-all n as))

(defn función-partition-all-3
  [n1 n2 as]
  (partition-all n1 n2 as))

(función-partition-all-1 1)
(función-partition-all-2 3 [0 1 2 3 4 5 6 7 8 9])
(función-partition-all-3 2 3 '(0 1 2 3 4 5 6 7 8 9))

(defn función-peek-1
  [as]
  (peek as))

(defn función-peek-2
  [as]
  (peek as))

(defn función-peek-3
  [as]
  (peek as))

(función-peek-1 [1 2 3 4 5])
(función-peek-2 (list 5 6 7 8 9 0 ))
(función-peek-3 [1 2 3 4 5 6 7 8 9 0 {0 0.2 0.3 0.4}])

(defn función-pop-1
  [as]
  (pop as))

(defn función-pop-2
  [as]
  (pop as))

(defn función-pop-3
  [as]
  (pop as))

(función-pop-1 [1 2 3 4 5])
(función-pop-2 (list 5 6 7 8 9 0))
(función-pop-3 [1 2 3 4 5 6 7 8 9 0 {0 0.2 0.3 0.4}])

(defn función-pos?-1
  [n]
  (pos? n))

(defn función-pos?-2
  [n]
  (pos? n))

(defn función-pos?-3
  [n]
  (pos? n))

(función-pos?-1 1)
(función-pos?-2 -1)
(función-pos?-3 6/7)

(defn función-quot-1
  [n n2]
  (quot n n2))

(defn función-quot-2
  [n n2]
  (quot n n2))

(defn función-quot-3
  [n n2]
  (quot n n2))

(función-quot-1 10 3)
(función-quot-2 -1 5)
(función-quot-3 6/7 2)

(defn función-range-1
  [n]
  (range n))

(defn función-range-2
  [n1 n2]
  (range n1 n2))

(defn función-range-3
  [n1 n2 n3]
  (range n1 n2 n3))

(función-range-1 8) 
(función-range-2 8 20)
(función-range-3 1 2 20)

(defn función-rem-1
  [n n2]
  (rem n n2))

(defn función-rem-2
  [n n2]
  (rem n n2))

(defn función-rem-3
  [n n2]
  (rem n n2))

(función-rem-1 15 5)
(función-rem-2 -15 2)
(función-rem-3 -5 -5)

(defn función-repeat-1
  [s]
  (repeat s)) 

(defn función-repeat-2
  [n s]
  (repeat n s))

(defn función-repeat-3
  [n s]
  (repeat n s))

(función-repeat-1 "hi")
(función-repeat-2 5 "hi")
(función-repeat-3 10 ":3")

(defn función-replace-1
  [as bs]
  (replace as bs))

(defn función-replace-2
  [as bs]
  (replace as bs))

(defn función-replace-3
  [as bs]
  (replace as bs))

(función-replace-1 [10 9 8 7 6] [6 8 4])
(función-replace-2 {"uno" 1, "dos" 2, :tres 3} [1])
(función-replace-3 {10 2}[])

(defn función-rest-1
  [as]
  (rest as))

(defn función-rest-2
  [as]
  (rest as))

(defn función-rest-3
  [as]
  (rest as))

(función-rest-1 [1 2 3 4 5 6 7 8 9])
(función-rest-2 {1 2 3 4 5 6})
(función-rest-3 (list 5 6 7 8 9 0))

(defn función-select-keys-1
  [as bs]
  (select-keys as bs))

(defn función-select-keys-2
  [as bs]
  (select-keys as bs))

(defn función-select-keys-3
  [as bs]
  (select-keys as bs))

(función-select-keys-1 {:a 1 :b 2} [:a])
(función-select-keys-2 {:a 1 :b 2} [:a :c])
(función-select-keys-3 [1 2 3] [0 3 2])

(defn función-shuffle-1
  [as]
  (shuffle as))

(defn función-shuffle-2
  [as]
  (shuffle as))

(defn función-shuffle-3
  [as]
  (shuffle as))

(función-shuffle-1 [1 2 3 4 5 6 7 8 9 10 11 12])
(función-shuffle-2 (list 1 2 3 4 5 6 7 8 9 10 11 12))
(función-shuffle-3 [\d \g 1 5 4])

(defn función-sort-1
  [as]
  (sort as))

(defn función-sort-2
  [as]
  (sort as))

(defn función-sort-3
  [as]
  (sort as))

(función-sort-1 [5 9 7 0 1 0.5])
(función-sort-2 {5 6, 1 2, 3 4})
(función-sort-3 #{5 6 7 8 9 0 1})

(defn función-split-at-1
  [n as]
  (split-at n as))

(defn función-split-at-2
  [n as]
  (split-at n as))

(defn función-split-at-3
  [n as]
  (split-at n as))

(función-split-at-1 2 [1 2 3 4 5])
(función-split-at-2 2 #{1 2 3 4 5 6 7 8 9})
(función-split-at-3 2 (list 1 2 3 4 5 6 7 8 9))

(defn función-str-1
  [n]
  (str n))

(defn función-str-2
  [s n]
  (str s n ))

(defn función-str-3
  [s as]
  (str s as 10))

(función-str-1 5)
(función-str-2 "oso" 45)
(función-str-3 {1 2 3 4} \h)

(defn función-subs-1
  [a b]
  (subs a b))

(defn función-subs-2
  [a b]
  (subs a b))

(defn función-subs-3
  [a b c]
  (subs a b c))

(función-subs-1 "Hola mundo" 4)
(función-subs-2 "Hola mundo" 9)
(función-subs-3 "Hola mundo" 1 3)

(defn función-subvec-1
  [vs a]
  (subvec vs a))

(defn función-subvec-2
  [vs a]
  (subvec vs a))

(defn función-subvec-3
  [vs a b]
  (subvec vs a b))

(función-subvec-1 [1 2 3 4 5] 3)
(función-subvec-2 ["a" "v" 4 7 \f] 1)
(función-subvec-3 [1 2 3 4 5]1 3)

(defn función-take-1
  [a as]
  (take a as))

(defn función-take-2
  [a as]
  (take a as))

(defn función-take-3
  [a as]
  (take a as))

(función-take-1 3 '(1 2 3 4 5 6))
(función-take-2 3 [1 2 3 4 5 6])
(función-take-3 3 [1 2])

(defn función-true?-1
  [a]
  (true? a))

(defn función-true?-2
  [a]
  (true? a))

(defn función-true?-3
  [a]
  (true? a))

(función-true?-1 (+ 5 -2))
(función-true?-2 (* -4 -5))
(función-true?-3 (/ -2 1))

(defn función-val-1
  [a]
  (val a))

(defn función-val-2
  [a]
  (val a))

(defn función-val-3
  [a]
  (val a))

(función-val-1 (first {:uno 1}))
(función-val-2 (first {:uno :dos}))
(función-val-3 (first {"hi" "bye"}))

(defn función-vals-1
  [as]
  (vals as))

(defn función-vals-2
  [as]
  (vals as))

(defn función-vals-3
  [as]
  (vals as))

(función-vals-1 {:a "hola" :b "adios" :c "bienvenido"})
(función-vals-2 {1 "hola", 2 "adios", 3 "bienvenido"})
(función-vals-3 {"hi" "hola"})

(defn función-zero?-1
  [a]
  (zero? a))

(defn función-zero?-2
  [a]
  ( zero? a))

(defn función-zero?-3
  [a]
  (zero? a))

(función-zero?-1 0)
(función-zero?-2 (* 5 5))
(función-zero?-3 (- 1.4 0.5 4.6))

(defn función-zipmap-1
  [as vs]
  (zipmap as vs))

(defn función-zipmap-2
  [as vs]
  (zipmap as vs))

(defn función-zipmap-3
  [as vs]
  (zipmap as vs))

(función-zipmap-1 [:a :b :c :d :e] [1 2 3 4 5])
(función-zipmap-2 [:a :b :c] [1 2])
(función-zipmap-3 [:a :b :c] [])